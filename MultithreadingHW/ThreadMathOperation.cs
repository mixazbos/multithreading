﻿using MultithreadingHW.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace MultithreadingHW
{
    public class ThreadMathOperation : IMathOperation
    {
        private static int _threadsCount;
        public ThreadMathOperation(int threadsCount)
        {
            _threadsCount = threadsCount;
        }

        public long SumListElements(List<int> numbers)
        {
            Stopwatch sw = new Stopwatch();

            var resultSum = 0;
            var idx = 0;
            var handlers = new WaitHandle[_threadsCount];

            var chunks = BreakIntoChunks(numbers, _threadsCount);

            sw.Start();

            foreach (var chunk in chunks)
            {
                handlers[idx] = new AutoResetEvent(false);

                var chunkIndex = idx;
                var thread = new Thread(() =>
                {
                    var sum = 0;
                    foreach (var value in chunk)
                    {
                        sum += value;
                    }

                    resultSum += sum;
                    var autoResetEvent = (AutoResetEvent)handlers[chunkIndex];
                    autoResetEvent.Set();
                });

                thread.Start();
                idx++;
            }

            WaitHandle.WaitAll(handlers);

            sw.Stop();
            Console.WriteLine($"Результат сложения: {resultSum}. Время выполнения: {sw.Elapsed.TotalMilliseconds} мс");

            return resultSum;
        }


        private static List<List<int>> BreakIntoChunks(List<int> numbers, int threadCount)
        {
            List<List<int>> res = new List<List<int>>();

            var partSize = numbers.Count / threadCount;

            if (numbers.Count < threadCount)
            {
                res.Add(numbers);
                return res;
            }

            List<int> buf = new List<int>();

            for (int i = 0; i < numbers.Count; i++)
            {
                if (i != 0 && i % partSize == 0 && res.Count != threadCount - 1)
                {
                    var chunk = new int[partSize];
                    buf.CopyTo(chunk, 0);
                    res.Add(chunk.ToList());
                    buf.Clear();
                }

                buf.Add(numbers[i]);

                if (res.Count == threadCount - 1 && i == numbers.Count - 1)
                {
                    res.Add(buf);
                }
            }

            return res;
        }
    }
}
