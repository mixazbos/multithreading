﻿using MultithreadingHW.Interface;
using System.Collections.Generic;
using System.Linq;

namespace MultithreadingHW
{
    public class PLINQMathOperation : IMathOperation
    {
        /// <summary>
        /// Сложение элементов
        /// </summary>
        /// <param name="numbers">Список чисел</param>
        /// <returns>Сумма</returns>
        public long SumListElements(List<int> numbers)
        {
            return numbers.AsParallel().Sum();
        }
    }
}
