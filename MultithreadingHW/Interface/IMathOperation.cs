﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultithreadingHW.Interface
{
    public interface IMathOperation
    {
        long SumListElements(List<int> elements);
    }
}
